# Runner test

Debugging something to do with Gitlab CI Runner

## Description

The CI is intended to validate the HTML file using the official docker image for Nu HTML Checker (https://github.com/validator/validator)

It is failing with: 

```
/bin/sh: 3: set: Illegal option -o pipefail
```

I found out that only `/bin/bash` supports the `pipefail` option. It seems like the runner expects `/bin/sh` 
to be a symlink to `/bin/bash` but it's not. 
